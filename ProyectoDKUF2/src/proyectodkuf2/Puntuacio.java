package proyectodkuf2;


public class Puntuacio implements Comparable {
	int puntuacio;
	String nombre;

	public Puntuacio(int puntuacio, String nombre) {
		super();
		this.puntuacio = puntuacio;
		this.nombre = nombre;
	}
	

	@Override
	public int compareTo(Object arg0) {
		Puntuacio otro = (Puntuacio) arg0;
		if(this.puntuacio!=otro.puntuacio) return this.puntuacio-otro.puntuacio;
		return puntuacio;
	}

	@Override
	public String toString() {
		return "Puntuacio [puntuacio=" + puntuacio + ", nombre=" + nombre + "]";
	}
}
