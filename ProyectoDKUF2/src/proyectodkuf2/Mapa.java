package proyectodkuf2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class Mapa implements Serializable {

	final static String Fsuelo = "media/images/stages/suelo.png";
	final static String Fescalera = "media/images/stages/escalera.png";
	final static String Fmartillo = "media/images/stages/martillo.png";

	static String[] barrilSprites = { "media/images/stages/barrilDer.gif", "media/images/stages/barrilIzq.gif" };
	static Barril b = new Barril("barril", 160, 248, 180, 268, barrilSprites);
	static ArrayList<Terreno> suelo = new ArrayList<Terreno>();
	static ArrayList<Rampa> subida = new ArrayList<Rampa>();
	static ArrayList<Escalera> escalera = new ArrayList<Escalera>();
	static ArrayList<Barril> barril = new ArrayList<Barril>();
	static ArrayList<NPC> npcs = new ArrayList<NPC>();
	static ArrayList<Martillo> martillos = new ArrayList<Martillo>();

	public static void nivel1Insert(int startX, int startY, int finishX, int finishY) {
		insertEscalerasSubibles(startX, startY, finishX, finishY);
		insertEscalerasNoSubibles(startX, startY, finishX, finishY);
		insertTerreno1(startX, startY, finishX, finishY);
		insertTerrenos(startX, startY, finishX, finishY);
		insertEscalerasInv();
		insertMartillos();
	}

	static void martilloTocado(Field f) {
		for (Martillo mm : martillos) {
			mm.tocado(f);
		}
	}

	private static void insertMartillos() {
		martillos.add(new Martillo("martillo", 464, 630, 480, 645, Fmartillo));
		martillos.add(new Martillo("martillo", 164, 310, 180, 325, Fmartillo));
	}

	public static void insertHUDandNPCs(int startX, int startY, int finishX, int finishY, int i, boolean ganado) {
		insertLifes(startX, startY, finishX, finishY, i);
		insertarNPCs(startX, startY, finishX, finishY, ganado);
	}

	static void gravedadB(Field f, int[] opcSelectArr) {
		for (Barril bb : barril) {
			bb.caidaBarril(f);
			bb.tocandoSuelo(f, opcSelectArr);
			bb.caidaEscalera(f);
		}

	}

	static void barriles() {
		b = new Barril("barril", 155, 248, 175, 268, barrilSprites);
		barril.add(b);
	}

	private static void insertarNPCs(int startX, int startY, int finishX, int finishY, boolean ganado) {
		// Peach
		if (!ganado) {
			startX = 192;
			startY = 136;
			finishX = 270;
			finishY = 184;
			npcs.add(new NPC("peach", startX, startY, finishX, finishY, "media/images/characters/peach.gif"));

			// DK
			startX = 74;
			startY = 204;
			finishX = 160;
			finishY = 268;
			npcs.add(new NPC("dk", startX, startY, finishX, finishY, "media/images/characters/dk.gif"));
		} else if (ganado) {
			// Peach
			startX = 192;
			startY = 136;
			finishX = 270;
			finishY = 184;
			npcs.add(new NPC("peach", startX, startY, finishX, finishY, "media/images/characters/peachWin.png"));

			// DK
			startX = 74;
			startY = 204;
			finishX = 160;
			finishY = 268;
			npcs.add(new NPC("dk", startX, startY, finishX, finishY, "media/images/characters/dkLost.gif"));
		}

	}

	private static void insertLifes(int startX, int startY, int finishX, int finishY, int i) {
		startX = 516;
		startY = 50;
		finishX = 532;
		finishY = 66;
		for (int j = 0; j < i; j++) {
			npcs.add(new NPC("life", startX, startY, finishX, finishY, "media/images/stages/life.png"));
			startX += 16;
			finishX += 16;
		}
	}

	private static void insertTerrenos(int startX, int startY, int finishX, int finishY) {
		// Segundo nivel terreno
		startX = 516;
		startY = 706;
		finishX = 548;
		finishY = 722;

		for (int i = 0; i < 3; i++) {
			suelo.add(new Terreno("suelo", startX, startY, finishX, finishY, Fsuelo));
			startX -= 32;
			finishX -= 32;
			startY -= 2;
			finishY -= 2;
		}
		for (int i = 0; i < 12; i++) {
			subida.add(new Rampa("rampa", startX, startY, finishX, finishY, Fsuelo));
			startX -= 32;
			finishX -= 32;
			startY -= 2;
			finishY -= 2;
		}
		// Tercer nivel terreno
		startX = 100;
		startY = 600;
		finishX = 132;
		finishY = 616;

		for (int i = 0; i < 3; i++) {
			suelo.add(new Terreno("suelo", startX, startY, finishX, finishY, Fsuelo));
			startX += 32;
			finishX += 32;
			startY -= 2;
			finishY -= 2;
		}
		for (int i = 0; i < 1; i++) {
			subida.add(new Rampa("rampa", startX, startY, finishX, finishY, Fsuelo));
			startX += 32;
			finishX += 32;
			startY -= 2;
			finishY -= 2;
		}
		for (int i = 0; i < 3; i++) {
			suelo.add(new Terreno("suelo", startX, startY, finishX, finishY, Fsuelo));
			startX += 32;
			finishX += 32;
			startY -= 2;
			finishY -= 2;
		}
		for (int i = 0; i < 8; i++) {
			subida.add(new Rampa("rampa", startX, startY, finishX, finishY, Fsuelo));
			startX += 32;
			finishX += 32;
			startY -= 2;
			finishY -= 2;
		}
		// Quarto nivel terreno
		startX = 516;
		startY = 494;
		finishX = 548;
		finishY = 510;

		for (int i = 0; i < 3; i++) {
			suelo.add(new Terreno("suelo", startX, startY, finishX, finishY, Fsuelo));
			startX -= 32;
			finishX -= 32;
			startY -= 2;
			finishY -= 2;
		}
		for (int i = 0; i < 3; i++) {
			subida.add(new Rampa("rampa", startX, startY, finishX, finishY, Fsuelo));
			startX -= 32;
			finishX -= 32;
			startY -= 2;
			finishY -= 2;
		}
		for (int i = 0; i < 3; i++) {
			suelo.add(new Terreno("rampa", startX, startY, finishX, finishY, Fsuelo));
			startX -= 32;
			finishX -= 32;
			startY -= 2;
			finishY -= 2;
		}
		for (int i = 0; i < 6; i++) {
			subida.add(new Rampa("rampa", startX, startY, finishX, finishY, Fsuelo));
			startX -= 32;
			finishX -= 32;
			startY -= 2;
			finishY -= 2;
		}

		// Quinto nivel terreno
		startX = 100;
		startY = 388;
		finishX = 132;
		finishY = 404;

		for (int i = 0; i < 3; i++) {
			suelo.add(new Terreno("suelo", startX, startY, finishX, finishY, Fsuelo));
			startX += 32;
			finishX += 32;
			startY -= 2;
			finishY -= 2;
		}
		for (int i = 0; i < 12; i++) {
			subida.add(new Rampa("rampa", startX, startY, finishX, finishY, Fsuelo));
			startX += 32;
			finishX += 32;
			startY -= 2;
			finishY -= 2;
		}

		// Ultimo nivel para victoria
		startX = 284;
		startY = 184;
		finishX = 316;
		finishY = 200;

		for (int i = 0; i < 4; i++) {
			suelo.add(new Terreno("suelo", startX, startY, finishX, finishY, Fsuelo));
			startX -= 32;
			finishX -= 32;
		}
	}

	private static void insertEscalerasNoSubibles(int startX, int startY, int finishX, int finishY) {
		// Terreno 1 a 2
		// Primera escalera
		startX = 200;
		startY = 766;
		finishX = 216;
		finishY = 800;
		escalera.add(new Escalera("escalera", startX, startY, finishX, finishY, Fescalera));
		// Terreno 3 a 4
		// Primera escalera
		startX = 172;
		startY = 562;
		finishX = 188;
		finishY = 596;
		escalera.add(new Escalera("escalera", startX, startY, finishX, finishY, Fescalera));
		// Terreno 4 a 5
		// Primera escalera
		startX = 436;
		startY = 454;
		finishX = 452;
		finishY = 488;
		escalera.add(new Escalera("escalera", startX, startY, finishX, finishY, Fescalera));
		// Terreno 5 a 6
		// Primera escalera
		startX = 272;
		startY = 344;
		finishX = 288;
		finishY = 378;
		escalera.add(new Escalera("escalera", startX, startY, finishX, finishY, Fescalera));

	}

	private static void insertEscalerasInv() {
		// Primer nivel
		// Primera escalera
		escalera.add(new Escalera("escaleraI", 496, 705, 512, 720, ""));

		// Segundo nivel
		// Primera escalera
		escalera.add(new Escalera("escaleraI", 266, 591, 282, 606, ""));
		// Segunda escalera
		escalera.add(new Escalera("escaleraI", 134, 599, 150, 614, ""));

		// Tercer nivel
		// Segunda escalera
		escalera.add(new Escalera("escaleraI", 306, 481, 322, 496, ""));
		// Tercera escalera
		escalera.add(new Escalera("escaleraI", 496, 493, 512, 508, ""));

		// Cuarto nivel
		// Segunda escalera
		escalera.add(new Escalera("escaleraI", 234, 381, 250, 396, ""));
		// Tercera escalera
		escalera.add(new Escalera("escaleraI", 134, 387, 150, 402, ""));

		// Quinto nivel
		// Segunda escalera
		escalera.add(new Escalera("escaleraI", 496, 281, 512, 296, ""));
	}

	private static void insertEscalerasSubibles(int startX, int startY, int finishX, int finishY) {
		// Terreno 1 a 2
		// Primera escalera
		startX = 496;
		startY = 754;
		finishX = 512;
		finishY = 788;
		for (int i = 0; i < 2; i++) {
			escalera.add(new Escalera("escalera", startX, startY, finishX, finishY, Fescalera));
			startY -= 34;
			finishY -= 34;
		}

		// Terreno 2 a 3
		// Primera escalera
		startX = 266;
		startY = 648;
		finishX = 282;
		finishY = 690;
		for (int i = 0; i < 2; i++) {
			escalera.add(new Escalera("escalera", startX, startY, finishX, finishY, Fescalera));
			startY -= 42;
			finishY -= 42;
		}
		// Segunda escalera
		startX = 134;
		startY = 648;
		finishX = 150;
		finishY = 682;
		for (int i = 0; i < 2; i++) {
			escalera.add(new Escalera("escalera", startX, startY, finishX, finishY, Fescalera));
			startY -= 34;
			finishY -= 34;
		}

		// Terreno 3 a 4
		// Segunda escalera
		startX = 306;
		startY = 556;
		finishX = 322;
		finishY = 588;
		for (int i = 0; i < 3; i++) {
			escalera.add(new Escalera("escalera", startX, startY, finishX, finishY, Fescalera));
			startY -= 30;
			finishY -= 30;
		}

		// Tercera esalera
		startX = 496;
		startY = 542;
		finishX = 512;
		finishY = 576;
		for (int i = 0; i < 2; i++) {
			escalera.add(new Escalera("escalera", startX, startY, finishX, finishY, Fescalera));
			startY -= 34;
			finishY -= 34;
		}

		// Terreno 4 a 5
		// Segunda esalera
		startX = 234;
		startY = 436;
		finishX = 250;
		finishY = 476;
		for (int i = 0; i < 2; i++) {
			escalera.add(new Escalera("escalera", startX, startY, finishX, finishY, Fescalera));
			startY -= 40;
			finishY -= 40;
		}
		// Tercera esalera
		startX = 134;
		startY = 436;
		finishX = 150;
		finishY = 470;
		for (int i = 0; i < 2; i++) {
			escalera.add(new Escalera("escalera", startX, startY, finishX, finishY, Fescalera));
			startY -= 34;
			finishY -= 34;
		}

		// Terreno 5 a 6
		// Segunda escalera
		startX = 496;
		startY = 330;
		finishX = 512;
		finishY = 364;
		for (int i = 0; i < 2; i++) {
			escalera.add(new Escalera("escalera", startX, startY, finishX, finishY, Fescalera));
			startY -= 34;
			finishY -= 34;
		}

		// Terreno 6 a victoria
		// Ultima escalera para vicoria
		startX = 292;
		startY = 234;
		finishX = 308;
		finishY = 268;
		for (int i = 0; i < 2; i++) {
			escalera.add(new Escalera("escalera", startX, startY, finishX, finishY, Fescalera));
			startY -= 34;
			finishY -= 34;
		}
	}

	private static void insertTerreno1(int startX, int startY, int finishX, int finishY) {
		// Primer nivel terreno
		startX = 68;
		startY = 800;
		finishX = 100;
		finishY = 816;
		for (int i = 0; i < 8; i++) {
			suelo.add(new Terreno("suelo", startX, startY, finishX, finishY, Fsuelo));
			startX += 32;
			finishX += 32;
		}
		for (int i = 0; i < 8; i++) {
			startY -= 2;
			finishY -= 2;
			subida.add(new Rampa("rampa", startX, startY, finishX, finishY, Fsuelo));
			startX += 32;
			finishX += 32;
		}

		// Ultimo nivel terreno
		startX = 516;
		startY = 282;
		finishX = 548;
		finishY = 298;
		for (int i = 0; i < 3; i++) {
			suelo.add(new Terreno("suelo", startX, startY, finishX, finishY, Fsuelo));
			startX -= 32;
			finishX -= 32;
			startY -= 2;
			finishY -= 2;
		}
		for (int i = 0; i < 4; i++) {
			subida.add(new Rampa("rampa", startX, startY, finishX, finishY, Fsuelo));
			startX -= 32;
			finishX -= 32;
			startY -= 2;
			finishY -= 2;
		}
		for (int i = 0; i < 8; i++) {
			subida.add(new Rampa("suelo", startX, startY, finishX, finishY, Fsuelo));
			startX -= 32;
			finishX -= 32;
		}
	}
}