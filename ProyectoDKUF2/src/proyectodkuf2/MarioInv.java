package proyectodkuf2;

import java.util.ArrayList;

public class MarioInv extends Aliados {

	char direccion = 'd';
	int cooldown = 30;
	int estat = 0; // 0 suelo, 1 subiendo 2 cayendo 3 escalando
	int maxSalto = 25;
	boolean tocSuelo = false;

	public MarioInv(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
	}

	public void moveIzq(Field f, int[] opcSelectArr) {
		if (estat == 0 || estat == 1 || estat == 2) {
			if(opcSelectArr[0]==0) {
				x1--;
				x2--;
			}
			else if(opcSelectArr[0]==2) {
				x1 -= 3;
				x2 -= 3;
			} else {
				x1 -= 2;
				x2 -= 2;
			}
			direccion = 'i';
		}
		if (estat == 3 && isGrounded(f)) {
			if(opcSelectArr[0]==0) {
				x1--;
				x2--;
			}
			else if(opcSelectArr[0]==2) {
				x1 -= 3;
				x2 -= 3;
			} else {
				x1 -= 2;
				x2 -= 2;
			}
			direccion = 'i';
			getGrounded(f);
			estat = 0;
		}
		
	}

	public void moveDer(Field f, int[] opcSelectArr) {
		if (estat == 0 || estat == 1 || estat == 2) {
			if(opcSelectArr[0]==0) {
				x1++;
				x2++;
			}
			else if(opcSelectArr[0]==2) {
				x1 += 3;
				x2 += 3;
			} else {
				x1 += 2;
				x2 += 2;
			}
			direccion = 'd';
		}
		if (estat == 3 && isGrounded(f)) {
			if(opcSelectArr[0]==0) {
				x1++;
				x2++;
			}
			else if(opcSelectArr[0]==2) {
				x1 += 3;
				x2 += 3;
			} else {
				x1 += 2;
				x2 += 2;
			}
			direccion = 'd';
			getGrounded(f);
			estat = 0;
		}
	}

	public void movv(Field f) {
		boolean test = false;
		ArrayList<Sprite> ls = collidesWithField(f);
		if (isGrounded(f) && estat != 3) {
			for (Sprite s : ls) {
				if (s instanceof Terreno) {
					if (this.stepsOn(s)) {
						getGrounded(f);
					}
				}
			}
			estat = 0;
			maxSalto = 25;

		} else if (estat != 3) {
			estat = 2;
			y1 = y1 + 2;
			y2 = y2 + 2;
			
		}
		tocSuelo = false;
		for (Sprite s : ls) {
			// System.out.println(s);
			if (s instanceof Escalera) {
				if (this.stepsOn(s)) {
					estat = 3;
					tocSuelo = true;
				}
			}
			if (s instanceof Rampa) {
				getGrounded(f);
			}
		}
	}

	public void down() {
		if (estat == 3) {
			y2 += 1;
			y1 += 1;
		}
	}

	public void escalar() {
		if (estat == 3 && tocSuelo) {
			y2 -= 1;
			y1 -= 1;
		}
	}

	public boolean barrilTocat(Field f) {
		// TODO Auto-generated method stub
		ArrayList<Sprite> ls = collidesWithField(f);
		for (Sprite s : ls) {
			//System.out.println(s);
			if (s instanceof Barril) {
				return true;
			}
		}
		return false;
	}

}
