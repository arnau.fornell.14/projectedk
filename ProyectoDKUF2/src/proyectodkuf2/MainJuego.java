package proyectodkuf2;

import java.awt.Font;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

public class MainJuego {

	static Field f = new Field();
	static Window w = new Window(f);
	static Random r = new Random();

	// static String[] barrilSprites = { "media/images/stages/barrilDer.gif",
	// "media/images/stages/barrilIzq.gif" };
	// static Barril b = new Barril("barril", 160, 248, 180, 268, barrilSprites);
	static String[] marioSprites = { /* Normal 0,1 */"media/images/characters/marioDer.gif",
			"media/images/characters/marioIzq.gif", /* Muriendo 2,3,4 */"media/images/characters/mDying1.png",
			"media/images/characters/marioDie.gif", "media/images/characters/mDying3.png",
			/* Escalando/bajando 5,6 */"media/images/characters/marioClimbing.gif",
			/* Martillo 7,8 */"media/images/characters/mHammerRight.gif", "media/images/characters/mHammerLeft.gif", };
	static Puntuacio punt = new Puntuacio(0, "");
	static Mario m = new Mario("Mario", 108, 768, 132, 800, marioSprites);
	static MarioInv mI = new MarioInv("MarioInv", 108, 768, 132, 800, "");
	static Texto t = new Texto("texto", 70, 50, 80, 60, "0");
	static NPC npc = new NPC(" ", 0, 0, 0, 0, " ");
	// static ArrayList<Terreno> suelo = new ArrayList<Terreno>();
	// static ArrayList<Rampa> subida = new ArrayList<Rampa>();
	// static ArrayList<Escalera> escalera = new ArrayList<Escalera>();
	// static ArrayList<Barril> barril = new ArrayList<Barril>();
	static ArrayList<Texto> fuentes = new ArrayList<Texto>();
	// static ArrayList<NPC> npcs = new ArrayList<NPC>();
	static ArrayList<ABC> rankings = new ArrayList<ABC>();
	static ArrayList<ABC> save = new ArrayList<ABC>();

	static int acc = 1;
	static boolean mar;

	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		int startX = 0;
		int startY = 0;
		int finishX = 0;
		int finishY = 0;
		boolean barrilToc = false;
		boolean barrilSaltat = false;
		int genBar = 100;
		boolean ganado = false;
		int lives = 3;
		int martilloTime = 0;
		boolean exit = false;
		int menuSelect = 1;
		int opcSelect = 1;
		int[] opcSelectArr = { 1, 1, 1 };

		// NAME SELECTOR
		String abc[] = { "A.png", "B.png", "C.png", "D.png", "E.png", "F.png", "G.png", "H.png", "I.png", "J.png",
				"K.png", "L.png", "M.png", "N.png", "O.png", "P.png", "Q.png", "R.png", "S.png", "T.png", "U.png",
				"V.png", "W.png", "X.png", "Y.png", "Z.png", "1.png", "2.png", "3.png", "4.png", "5.png", "6.png",
				"7.png", "8.png", "9.png", "0.png" };
		String abcb[] = { "Ab.png", "Bb.png", "Cb.png", "Db.png", "Eb.png", "Fb.png", "Gb.png", "Hb.png", "Ib.png",
				"Jb.png", "Kb.png", "Lb.png", "Mb.png", "Nb.png", "Ob.png", "Pb.png", "Qb.png", "Rb.png", "Sb.png",
				"Tb.png", "Ub.png", "Vb.png", "Wb.png", "Xb.png", "Yb.png", "Zb.png" };

		// MENU SPRITES
		String[] menuSprites = { /* On 0,1,2,3,4 */"media/images/abc/EPOn.png", "media/images/abc/VROn.png",
				"media/images/abc/OOn.png", "media/images/abc/COOn.png", "media/images/abc/SOn.png",
				/* Off 5,6,7,8,9 */"media/images/abc/EPOff.png", "media/images/abc/VROff.png",
				"media/images/abc/OOff.png", "media/images/abc/COOff.png", "media/images/abc/SOff.png",
				"media/images/abc/T.png", /* cargar 11 12 */ "media/images/abc/CPON.png",
				"media/images/abc/CPOff.png" };

		// RANKING SYSTEM
		int letSel = 0;
		int accRank = 0;
		String[] nom = new String[3];
		int nomFin = 0;
		char pressed = '*';


		File fileR = new File("recources/ranking.csv");
		w.playMusic("media/sounds/mm.wav");
		while (!exit) {
			boolean selected = false;
			Menu Title = new Menu("title", 35, 50, 630, 250, menuSprites);
			Menu Empezar = new Menu("empezar", 35, 300, 630, 338, menuSprites);
			Menu Cargar = new Menu("cargar", 35, 400, 630, 438, menuSprites);
			Menu Rankings = new Menu("ranks", 79, 500, 586, 538, menuSprites);
			Menu Opciones = new Menu("opciones", 165, 600, 500, 638, menuSprites);
			Menu Controles = new Menu("controles", 144, 700, 511, 738, menuSprites);
			Menu ExitGame = new Menu("salir", 230, 800, 435, 838, menuSprites);
			Title.currentImg = 10;
			Empezar.currentImg = 0;
			Cargar.currentImg = 12;
			Rankings.currentImg = 6;
			Opciones.currentImg = 7;
			Controles.currentImg = 8;
			ExitGame.currentImg = 9;
			menuSelect = 1;
			boolean pressedM = false;
			while (!selected) {
				ArrayList<Sprite> mainMenu = new ArrayList<Sprite>();
				mainMenu.add(Title);
				mainMenu.add(Empezar);
				mainMenu.add(Cargar);
				mainMenu.add(Rankings);
				mainMenu.add(Opciones);
				mainMenu.add(Controles);
				mainMenu.add(ExitGame);
				// BAJAR EN EL MENU
				if (w.getPressedKeys().isEmpty()) {
					pressedM = false;
				} else if (!pressedM) {
					if (w.getPressedKeys().contains('s') && menuSelect == 1) {
						menuSelect = 2;
						Empezar.currentImg = 5;
						Cargar.currentImg = 11;
						Rankings.currentImg = 6;
						Opciones.currentImg = 7;
						Controles.currentImg = 8;
						ExitGame.currentImg = 9;
					} else if (w.getPressedKeys().contains('s') && menuSelect == 2) {
						menuSelect = 3;
						Empezar.currentImg = 5;
						Cargar.currentImg = 12;
						Rankings.currentImg = 1;
						Opciones.currentImg = 7;
						Controles.currentImg = 8;
						ExitGame.currentImg = 9;
					} else if (w.getPressedKeys().contains('s') && menuSelect == 3) {
						menuSelect = 4;
						Empezar.currentImg = 5;
						Cargar.currentImg = 12;
						Rankings.currentImg = 6;
						Opciones.currentImg = 2;
						Controles.currentImg = 8;
						ExitGame.currentImg = 9;

					} else if (w.getPressedKeys().contains('s') && menuSelect == 4) {
						menuSelect = 5;
						Empezar.currentImg = 5;
						Cargar.currentImg = 12;
						Rankings.currentImg = 6;
						Opciones.currentImg = 7;
						Controles.currentImg = 3;
						ExitGame.currentImg = 9;

					} else if (w.getPressedKeys().contains('s') && menuSelect == 5) {
						menuSelect = 6;
						Empezar.currentImg = 5;
						Cargar.currentImg = 12;
						Rankings.currentImg = 6;
						Opciones.currentImg = 7;
						Controles.currentImg = 8;
						ExitGame.currentImg = 4;

					} else if (w.getPressedKeys().contains('s') && menuSelect == 6) {
						menuSelect = 1;
						Empezar.currentImg = 0;
						Cargar.currentImg = 12;
						Rankings.currentImg = 6;
						Opciones.currentImg = 7;
						Controles.currentImg = 8;
						ExitGame.currentImg = 9;

					}
					// SUBIR EN EL MENU
					if (w.getPressedKeys().contains('w') && menuSelect == 1) {
						menuSelect = 6;
						Empezar.currentImg = 5;
						Cargar.currentImg = 12;
						Rankings.currentImg = 6;
						Opciones.currentImg = 7;
						Controles.currentImg = 8;
						ExitGame.currentImg = 4;
					} else if (w.getPressedKeys().contains('w') && menuSelect == 3) {
						menuSelect = 2;
						Empezar.currentImg = 5;
						Cargar.currentImg = 11;
						Rankings.currentImg = 6;
						Opciones.currentImg = 7;
						Controles.currentImg = 8;
						ExitGame.currentImg = 9;
					} else if (w.getPressedKeys().contains('w') && menuSelect == 2) {
						menuSelect = 1;
						Empezar.currentImg = 0;
						Cargar.currentImg = 12;
						Rankings.currentImg = 6;
						Opciones.currentImg = 7;
						Controles.currentImg = 8;
						ExitGame.currentImg = 9;
					} else if (w.getPressedKeys().contains('w') && menuSelect == 4) {
						menuSelect = 3;
						Empezar.currentImg = 5;
						Cargar.currentImg = 12;
						Rankings.currentImg = 1;
						Opciones.currentImg = 7;
						Controles.currentImg = 8;
						ExitGame.currentImg = 9;
					} else if (w.getPressedKeys().contains('w') && menuSelect == 5) {
						menuSelect = 4;
						Empezar.currentImg = 5;
						Cargar.currentImg = 12;
						Rankings.currentImg = 6;
						Opciones.currentImg = 2;
						Controles.currentImg = 8;
						ExitGame.currentImg = 9;
					} else if (w.getPressedKeys().contains('w') && menuSelect == 6) {
						menuSelect = 5;
						Empezar.currentImg = 5;
						Cargar.currentImg = 12;
						Rankings.currentImg = 6;
						Opciones.currentImg = 7;
						Controles.currentImg = 3;
						ExitGame.currentImg = 9;
					}
					// SELECIONAR OPCION
					if (w.getPressedKeys().contains(' ')) {
						selected = true;
					}
					pressedM = true;
				}
				f.draw(mainMenu);
				try {
					Thread.sleep(20);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			switch (menuSelect) {
			case 1:
				Mapa.nivel1Insert(startX, startY, finishX, finishY);
				w.stopMusic();
				boolean pressedP = false;
				for (int i = lives; lives > 0; lives--) {
					w.playMusic("media/sounds/bkMusic.wav");
					Mapa.insertHUDandNPCs(startX, startY, finishX, finishY, lives, ganado);
					// howHigh(startX, startY, finishX, finishY);
					// insertHUDandNPCs(startX, startY, finishX, finishX, i, ganado);
					boolean pressedOne = false;
					while (!barrilToc && !ganado && !pressedOne) {
						if (w.getPressedKeys().contains('1'))
							pressedOne = true;
						input(pressedP, opcSelectArr,lives);
						sounds();
						barrilToc = gravedad(barrilToc);
						Mapa.gravedadB(f, opcSelectArr);
						punt.puntuacio = sumarPunto(barrilSaltat, opcSelectArr);
						Font fuentePunt = new Font("Sans", Font.BOLD, 22);
						int color = 0xFFFFFF;
						t.font = fuentePunt;
						t.textColor = color;
						ArrayList<Sprite> sp = new ArrayList<Sprite>();
						for (Iterator iterator = Mapa.barril.iterator(); iterator.hasNext();) {
							Sprite s = (Sprite) iterator.next();
							if (s.delete) {
								iterator.remove();
								// System.out.println("deleted");
							}
						}
						for (Iterator iterator = Mapa.martillos.iterator(); iterator.hasNext();) {
							Sprite s = (Sprite) iterator.next();
							if (s.delete) {
								iterator.remove();
								// System.out.println("deleted");
							}
						}
						martilloForm(martilloTime);
						Mapa.martilloTocado(f);
						if (martilloTime < 500 && m.martillo) {
							martilloTime++;
						} else {
							m.martillo = false;
							martilloTime = 0;
						}
						// System.out.println(opcSelectArr[2]);
						if (genBar == 200 && opcSelectArr[2] == 1) {
							Mapa.barriles();
							genBar = 0;
						} else if (genBar == 100 && opcSelectArr[2] == 2) {
							Mapa.barriles();
							genBar = 0;
						} else if (genBar == 300 && opcSelectArr[2] == 0) {
							Mapa.barriles();
							genBar = 0;
						}

						genBar++;
						ganado = peachSalvada();
						sp = addSprites(sp);
						m.update();
						f.draw(sp);
						try {
							Thread.sleep(20);
						} catch (InterruptedException e) {
						}
					}
					if (pressedOne) {
					} else if (ganado) {
						levelPassed(startX, startY, finishX, finishY, lives, ganado);
						String[] nomRank = ponerNombre(nomFin, pressed, nom, accRank, letSel, abc, abcb, accRank);
						insertarRanking(fileR, nomRank);
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
						}
						break;
					} else if (!ganado) {
						levelLost();
					}
					ganado = false;
					barrilToc = false;
				}
				if (ganado) {
					System.out
							.println("Has ganado con " + punt.puntuacio + " puntos y con " + lives + " vida/s restante/s.");
				}
				genBar = 100;
				lives=3;
				punt.puntuacio = 0;
				resetMap();

				w.playMusic("media/sounds/mm.wav");
				break;
			case 2:
				File fc = new File("recources/savegame.sv");
				FileInputStream fis = new FileInputStream(fc);
				ObjectInputStream ois = new ObjectInputStream(fis);
				Object om = ois.readObject();
				m = (Mario) om;
				Object omi = ois.readObject();
				mI = (MarioInv) omi;
				Object ot = ois.readObject();
				t = (Texto) ot;
				Object omb = ois.readObject();
				Mapa.barril = (ArrayList<Barril>) omb;
				Object op = ois.readObject();
				punt.puntuacio = (int) op;
				Object ops = ois.readObject();
				opcSelectArr = (int[]) ops;
				Object oi = ois.readObject();
				lives =  (int) oi;
				for (Barril bb : Mapa.barril) {
					if (bb.dir && bb.estat != 0)
						bb.dir = false;
					else if (!bb.dir && bb.estat != 0)
						bb.dir = true;
				}
				ois.close();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
				break;
			case 3:
				FileReader fr = new FileReader(fileR);
				BufferedReader br = new BufferedReader(fr);
				ArrayList<String> puntR = new ArrayList<String>();
				ArrayList<String> nombreR = new ArrayList<String>();
				while (br.ready()) {
					String s = br.readLine();
					String[] spl = s.split(";");
					puntR.add(spl[0]);
					nombreR.add(spl[1]);
					// System.out.println(spl[1].charAt(0));
					// System.out.println(spl[1].charAt(2));
					// System.out.println(spl[1].charAt(4));
				}
				br.close();

				while (!w.getPressedKeys().contains('1')) {
					ArrayList<Sprite> sp = new ArrayList<Sprite>();
					sp = addRanking(puntR, nombreR, abc, abcb);
					f.draw(sp);
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
					}
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
				break;
			case 4:
				String[] menuOpc = { /* titles 0,1,2 */"media/images/abc/VM.png", "media/images/abc/VB.png",
						"media/images/abc/VGB.png", /* opciones on, 3,4,5 */"media/images/abc/LNOn.png",
						"media/images/abc/NROn.png", "media/images/abc/RPOn.png",
						/* opciones off 6,7,8 */"media/images/abc/LNOff.png", "media/images/abc/NROff.png",
						"media/images/abc/RPOff.png" };
				Menu VelMario = new Menu("velmario", 165, 136, 500, 174, menuOpc);
				Menu SelVelMario = new Menu("velgenbarr", 230, 186, 435, 224, menuOpc);
				Menu VelBarr = new Menu("velmario", 144, 336, 511, 374, menuOpc);
				Menu SelVelBarr = new Menu("velgenbarr", 230, 386, 435, 424, menuOpc);
				Menu VelGenBarr = new Menu("velgenbarr", 80, 536, 585, 574, menuOpc);
				Menu SelVelGenBarr = new Menu("velgenbarr", 230, 586, 435, 624, menuOpc);
				VelMario.currentImg = 0;
				VelBarr.currentImg = 1;
				VelGenBarr.currentImg = 2;
				SelVelMario.currentImg = 4;
				SelVelBarr.currentImg = 7;
				SelVelGenBarr.currentImg = 7;
				opcSelectArr[0] = 1;
				opcSelectArr[1] = 1;
				opcSelectArr[2] = 1;
				opcSelect = 1;
				boolean pressedO = false;

				while (!w.getPressedKeys().contains('1')) {
					ArrayList<Sprite> opcMenu = new ArrayList<Sprite>();
					opcMenu.add(VelMario);
					opcMenu.add(VelBarr);
					opcMenu.add(VelGenBarr);
					opcMenu.add(SelVelMario);
					opcMenu.add(SelVelBarr);
					opcMenu.add(SelVelGenBarr);
					if (w.getPressedKeys().isEmpty()) {
						pressedO = false;
					} else if (!pressedO) {
						// SUBIR EN EL MENU
						if (w.getPressedKeys().contains('w') && opcSelect == 1) {
							opcSelect = 3;
							if (opcSelectArr[0] == 0) {
								SelVelMario.currentImg = 6;
							} else if (opcSelectArr[0] == 1) {
								SelVelMario.currentImg = 7;
							} else if (opcSelectArr[0] == 2) {
								SelVelMario.currentImg = 8;
							}
							if (opcSelectArr[2] == 0) {
								SelVelGenBarr.currentImg = 3;
							} else if (opcSelectArr[2] == 1) {
								SelVelGenBarr.currentImg = 4;
							} else if (opcSelectArr[2] == 2) {
								SelVelGenBarr.currentImg = 5;
							}
						} else if (w.getPressedKeys().contains('w') && opcSelect == 3) {
							opcSelect = 2;
							if (opcSelectArr[1] == 0) {
								SelVelBarr.currentImg = 3;
							} else if (opcSelectArr[1] == 1) {
								SelVelBarr.currentImg = 4;
							} else if (opcSelectArr[1] == 2) {
								SelVelBarr.currentImg = 5;
							}
							if (opcSelectArr[2] == 0) {
								SelVelGenBarr.currentImg = 6;
							} else if (opcSelectArr[2] == 1) {
								SelVelGenBarr.currentImg = 7;
							} else if (opcSelectArr[2] == 2) {
								SelVelGenBarr.currentImg = 8;
							}

						} else if (w.getPressedKeys().contains('w') && opcSelect == 2) {
							opcSelect = 1;
							if (opcSelectArr[0] == 0) {
								SelVelMario.currentImg = 3;
							} else if (opcSelectArr[0] == 1) {
								SelVelMario.currentImg = 4;
							} else if (opcSelectArr[0] == 2) {
								SelVelMario.currentImg = 5;
							}
							if (opcSelectArr[1] == 0) {
								SelVelBarr.currentImg = 6;
							} else if (opcSelectArr[1] == 1) {
								SelVelBarr.currentImg = 7;
							} else if (opcSelectArr[1] == 2) {
								SelVelBarr.currentImg = 8;
							}

						}
						// BAJAR EN EL MENU
						if (w.getPressedKeys().contains('s') && opcSelect == 1) {
							opcSelect = 2;
							if (opcSelectArr[0] == 0) {
								SelVelMario.currentImg = 6;
							} else if (opcSelectArr[0] == 1) {
								SelVelMario.currentImg = 7;
							} else if (opcSelectArr[0] == 2) {
								SelVelMario.currentImg = 8;
							}
							if (opcSelectArr[1] == 0) {
								SelVelBarr.currentImg = 3;
							} else if (opcSelectArr[1] == 1) {
								SelVelBarr.currentImg = 4;
							} else if (opcSelectArr[1] == 2) {
								SelVelBarr.currentImg = 5;
							}

						} else if (w.getPressedKeys().contains('s') && opcSelect == 3) {
							opcSelect = 1;
							if (opcSelectArr[0] == 0) {
								SelVelMario.currentImg = 3;
							} else if (opcSelectArr[0] == 1) {
								SelVelMario.currentImg = 4;
							} else if (opcSelectArr[0] == 2) {
								SelVelMario.currentImg = 5;
							}
							if (opcSelectArr[2] == 0) {
								SelVelGenBarr.currentImg = 6;
							} else if (opcSelectArr[2] == 1) {
								SelVelGenBarr.currentImg = 7;
							} else if (opcSelectArr[2] == 2) {
								SelVelGenBarr.currentImg = 8;
							}

						} else if (w.getPressedKeys().contains('s') && opcSelect == 2) {
							opcSelect = 3;
							if (opcSelectArr[2] == 0) {
								SelVelGenBarr.currentImg = 3;
							} else if (opcSelectArr[2] == 1) {
								SelVelGenBarr.currentImg = 4;
							} else if (opcSelectArr[2] == 2) {
								SelVelGenBarr.currentImg = 5;
							}
							if (opcSelectArr[1] == 0) {
								SelVelBarr.currentImg = 6;
							} else if (opcSelectArr[1] == 1) {
								SelVelBarr.currentImg = 7;
							} else if (opcSelectArr[1] == 2) {
								SelVelBarr.currentImg = 8;
							}

						}

						// ELEGIR IZQUIERDA, MARIO
						if (w.getPressedKeys().contains('a') && opcSelect == 1 && opcSelectArr[0] == 1) {
							opcSelectArr[0] = 0;
							SelVelMario.currentImg = 3;

						} else if (w.getPressedKeys().contains('a') && opcSelect == 1 && opcSelectArr[0] == 0) {
							opcSelectArr[0] = 2;
							SelVelMario.currentImg = 5;

						} else if (w.getPressedKeys().contains('a') && opcSelect == 1 && opcSelectArr[0] == 2) {
							opcSelectArr[0] = 1;
							SelVelMario.currentImg = 4;

						}
						// ELEGIR DERECHA, MARIO
						if (w.getPressedKeys().contains('d') && opcSelect == 1 && opcSelectArr[0] == 1) {
							opcSelectArr[0] = 2;
							SelVelMario.currentImg = 5;

						} else if (w.getPressedKeys().contains('d') && opcSelect == 1 && opcSelectArr[0] == 0) {
							opcSelectArr[0] = 1;
							SelVelMario.currentImg = 4;

						} else if (w.getPressedKeys().contains('d') && opcSelect == 1 && opcSelectArr[0] == 2) {
							opcSelectArr[0] = 0;
							SelVelMario.currentImg = 3;

						}
						// ELEGIR IZQUIERDA, VEL BARRIL
						if (w.getPressedKeys().contains('a') && opcSelect == 2 && opcSelectArr[1] == 1) {
							opcSelectArr[1] = 0;
							SelVelBarr.currentImg = 3;

						} else if (w.getPressedKeys().contains('a') && opcSelect == 2 && opcSelectArr[1] == 0) {
							opcSelectArr[1] = 2;
							SelVelBarr.currentImg = 5;

						} else if (w.getPressedKeys().contains('a') && opcSelect == 2 && opcSelectArr[1] == 2) {
							opcSelectArr[1] = 1;
							SelVelBarr.currentImg = 4;

						}
						// ELEGIR DERECHA, VEL BARRIL
						if (w.getPressedKeys().contains('d') && opcSelect == 2 && opcSelectArr[1] == 1) {
							opcSelectArr[1] = 2;
							SelVelBarr.currentImg = 5;

						} else if (w.getPressedKeys().contains('d') && opcSelect == 2 && opcSelectArr[1] == 0) {
							opcSelectArr[1] = 1;
							SelVelBarr.currentImg = 4;

						} else if (w.getPressedKeys().contains('d') && opcSelect == 2 && opcSelectArr[1] == 2) {
							opcSelectArr[1] = 0;
							SelVelBarr.currentImg = 3;

						}
						// ELEGIR IZQUIERDA, VEL GEN BARRIL
						if (w.getPressedKeys().contains('a') && opcSelect == 3 && opcSelectArr[2] == 1) {
							opcSelectArr[2] = 0;
							SelVelGenBarr.currentImg = 3;

						} else if (w.getPressedKeys().contains('a') && opcSelect == 3 && opcSelectArr[2] == 0) {
							opcSelectArr[2] = 2;
							SelVelGenBarr.currentImg = 5;

						} else if (w.getPressedKeys().contains('a') && opcSelect == 3 && opcSelectArr[2] == 2) {
							opcSelectArr[2] = 1;
							SelVelGenBarr.currentImg = 4;

						}
						// ELEGIR DERECHA, VEL GEN BARRILS
						if (w.getPressedKeys().contains('d') && opcSelect == 3 && opcSelectArr[2] == 1) {
							opcSelectArr[2] = 2;
							SelVelGenBarr.currentImg = 5;
						} else if (w.getPressedKeys().contains('d') && opcSelect == 3 && opcSelectArr[2] == 0) {
							opcSelectArr[2] = 1;
							SelVelGenBarr.currentImg = 4;

						} else if (w.getPressedKeys().contains('d') && opcSelect == 3 && opcSelectArr[2] == 2) {
							opcSelectArr[2] = 0;
							SelVelGenBarr.currentImg = 3;

						}
						pressedO = true;
					}
					f.draw(opcMenu);
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
					}
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
				break;
			case 5:
				while (!w.getPressedKeys().contains('1')) {
					ArrayList<Sprite> sp = new ArrayList<Sprite>();
					ABC cont = new ABC("ABC", 0, 156, 665, 702, "media/images/abc/controles.png");
					sp.add(cont);
					f.draw(sp);
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
					}
				}
				break;
			case 6:
				System.exit(0);
			}

		}
	}

	private static void resetMap() {
		// TODO Auto-generated method stub
		m = new Mario("Mario", 108, 768, 132, 800, marioSprites);
		mI = new MarioInv("MarioInv", 100, 767, 132, 800, "");
		t = new Texto("texto", 70, 50, 80, 60, "0");
		acc = 1;
		for (Barril bb : Mapa.barril) {
			bb.delete();
		}
		for (NPC npccss : Mapa.npcs) {
			npccss.delete();
		}
		// Delete all barrels
		for (Iterator iterator = Mapa.barril.iterator(); iterator.hasNext();) {
			Sprite s = (Sprite) iterator.next();
			if (s.delete) {
				iterator.remove();
				// System.out.println("deleted");
			}
		}
		// Delete all map
		for (Iterator iterator = Mapa.suelo.iterator(); iterator.hasNext();) {
			Sprite s = (Sprite) iterator.next();
			iterator.remove();
			// System.out.println("deleted"); d
		}
		for (Iterator iterator = Mapa.subida.iterator(); iterator.hasNext();) {
			Sprite s = (Sprite) iterator.next();
			iterator.remove();
			// System.out.println("deleted");
		}
		for (Iterator iterator = Mapa.escalera.iterator(); iterator.hasNext();) {
			Sprite s = (Sprite) iterator.next();
			iterator.remove();
			// System.out.println("deleted");
		}
		for (Iterator iterator = Mapa.barril.iterator(); iterator.hasNext();) {
			Sprite s = (Sprite) iterator.next();
			iterator.remove();
			// System.out.println("deleted");
		}
		for (Iterator iterator = Mapa.martillos.iterator(); iterator.hasNext();) {
			Sprite s = (Sprite) iterator.next();
			iterator.remove();
			// System.out.println("deleted");
		}

	}

	private static ArrayList<Sprite> addRanking(ArrayList<String> puntR, ArrayList<String> nombreR, String[] abc, String[] abcb) throws IOException {
		ArrayList<Sprite> ranksOrdened = new ArrayList<Sprite>();
		int startX = 0;
		int startY = 0;
		int finishX = 0;
		int finishY = 0;
		// letras
		startY = 25;
		finishY = 101;
		for (int j = 0; j < 5; j++) {
			nombreR.get(j);
			// System.out.println(nombreR.get(j));
			startX = 25;
			finishX = 82;
			for (int i = 0; i < 5; i++) {
				Sprite test = anadirLetras(i, j, nombreR, abc, startX, startY, finishX, finishY);
				if (test == null) {
				} else {
					ranksOrdened.add(test);
					startX += 76;
					finishX += 76;
				}
			}
			startY += 150;
			finishY += 150;
		}
		// numeros
		startY = 25;
		finishY = 101;
		for (int j = 0; j < 5; j++) {
			puntR.get(j);
			if (puntR.get(j).length() == 3) {
				startX = 357;
				finishX = 414;
				for (int i = 0; i < 3; i++) {
					ranksOrdened.add(anadirNumeros(i, j, puntR, abc, startX, startY, finishX, finishY));
					startX += 57;
					finishX += 57;
				}
			} else if (puntR.get(j).length() == 4) {
				startX = 357;
				finishX = 414;
				for (int i = 0; i < 4; i++) {
					ranksOrdened.add(anadirNumeros(i, j, puntR, abc, startX, startY, finishX, finishY));
					startX += 57;
					finishX += 57;
				}
			}
			startY += 150;
			finishY += 150;
		}
		return ranksOrdened;
	}

	private static Sprite anadirLetras(int i, int j, ArrayList<String> puntR, String[] abc, int startX, int startY,
			int finishX, int finishY) {
		ABC tre = null;
		// numeros
		tre = new ABC("ABC", startX, startY, finishX, finishY, abc);

		if (puntR.get(j).charAt(i) == 'A') {
			tre.currentImg = 0;
		} else if (puntR.get(j).charAt(i) == 'B') {
			tre.currentImg = 1;
		} else if (puntR.get(j).charAt(i) == 'C') {
			tre.currentImg = 2;
		} else if (puntR.get(j).charAt(i) == 'D') {
			tre.currentImg = 3;
		} else if (puntR.get(j).charAt(i) == 'E') {
			tre.currentImg = 4;
		} else if (puntR.get(j).charAt(i) == 'F') {
			tre.currentImg = 5;
		} else if (puntR.get(j).charAt(i) == 'G') {
			tre.currentImg = 6;
		} else if (puntR.get(j).charAt(i) == 'H') {
			tre.currentImg = 7;
		} else if (puntR.get(j).charAt(i) == 'I') {
			tre.currentImg = 8;
		} else if (puntR.get(j).charAt(i) == 'J') {
			tre.currentImg = 9;
		} else if (puntR.get(j).charAt(i) == 'K') {
			tre.currentImg = 10;
		} else if (puntR.get(j).charAt(i) == 'L') {
			tre.currentImg = 11;
		} else if (puntR.get(j).charAt(i) == 'M') {
			tre.currentImg = 12;
		} else if (puntR.get(j).charAt(i) == 'N') {
			tre.currentImg = 13;
		} else if (puntR.get(j).charAt(i) == 'O') {
			tre.currentImg = 14;
		} else if (puntR.get(j).charAt(i) == 'P') {
			tre.currentImg = 15;
		} else if (puntR.get(j).charAt(i) == 'Q') {
			tre.currentImg = 16;
		} else if (puntR.get(j).charAt(i) == 'R') {
			tre.currentImg = 17;
		} else if (puntR.get(j).charAt(i) == 'S') {
			tre.currentImg = 18;
		} else if (puntR.get(j).charAt(i) == 'T') {
			tre.currentImg = 19;
		} else if (puntR.get(j).charAt(i) == 'U') {
			tre.currentImg = 20;
		} else if (puntR.get(j).charAt(i) == 'V') {
			tre.currentImg = 21;
		} else if (puntR.get(j).charAt(i) == 'W') {
			tre.currentImg = 22;
		} else if (puntR.get(j).charAt(i) == 'X') {
			tre.currentImg = 23;
		} else if (puntR.get(j).charAt(i) == 'Y') {
			tre.currentImg = 24;
		} else if (puntR.get(j).charAt(i) == 'Z') {
			tre.currentImg = 25;
		} else if (puntR.get(j).charAt(i) == ' ') {
			return null;
		}
		return tre;
	}

	private static Sprite anadirNumeros(int i, int j, ArrayList<String> puntR, String[] abc, int startX, int startY,
			int finishX, int finishY) {
		ABC tre = null;
		// numeros
		tre = new ABC("ABC", startX, startY, finishX, finishY, abc);
		if (puntR.get(j).charAt(i) == '1') {
			tre.currentImg = 26;
		} else if (puntR.get(j).charAt(i) == '2') {
			tre.currentImg = 27;
		} else if (puntR.get(j).charAt(i) == '3') {
			tre.currentImg = 28;
		} else if (puntR.get(j).charAt(i) == '4') {
			tre.currentImg = 29;
		} else if (puntR.get(j).charAt(i) == '5') {
			tre.currentImg = 30;
		} else if (puntR.get(j).charAt(i) == '6') {
			tre.currentImg = 31;
		} else if (puntR.get(j).charAt(i) == '7') {
			tre.currentImg = 32;
		} else if (puntR.get(j).charAt(i) == '8') {
			tre.currentImg = 33;
		} else if (puntR.get(j).charAt(i) == '9') {
			tre.currentImg = 34;
		} else if (puntR.get(j).charAt(i) == '0') {
			tre.currentImg = 35;
		}
		return tre;
	}

	// TODO
	/*
	 * private static void howHigh(int startX, int startY, int finishX, int finishY)
	 * { startX = 268; startY = 642; finishX = 396; finishY = 770; Mapa.npcs.add(new
	 * NPC("dk", startX, startY, finishX, finishY,
	 * "media/images/characters/dkHigh.png"));
	 * 
	 * f.draw(Mapa.npcs); }
	 */

	private static String[] ponerNombre(int nomFin, char pressed, String[] nom, int acc2, int letSel, String[] abc,
			String[] abcb, int accRank) throws InterruptedException {
		deleteSave();
		deleteRank();
		accRank = 0;
		while (nomFin != 4) {
			int startX = 100 + accRank;
			int startY = 100;
			int finishX = 176 + accRank;
			int finishY = 176;
			ArrayList<Sprite> sp = new ArrayList<Sprite>();
			if (nomFin != 3) {
				if (letSel == 0) {
					ABC uno = new ABC("ABC", startX, startY, finishX, finishY, abcb);
					uno.currentImg = 25;
					finishY += 100;
					startY += 100;
					rankings.add(uno);
					ABC dos = new ABC("ABC", startX, startY, finishX, finishY, abc);
					dos.currentImg = letSel;
					finishY += 100;
					startY += 100;
					rankings.add(dos);
					ABC tre = new ABC("ABC", startX, startY, finishX, finishY, abcb);
					tre.currentImg = letSel + 1;
					rankings.add(tre);
				} else if (letSel == 25) {
					ABC uno = new ABC("ABC", startX, startY, finishX, finishY, abcb);
					uno.currentImg = letSel - 1;
					finishY += 100;
					startY += 100;
					rankings.add(uno);
					ABC dos = new ABC("ABC", startX, startY, finishX, finishY, abc);
					dos.currentImg = letSel;
					finishY += 100;
					startY += 100;
					rankings.add(dos);
					ABC tre = new ABC("ABC", startX, startY, finishX, finishY, abcb);
					tre.currentImg = 0;
					rankings.add(tre);
				} else {
					ABC uno = new ABC("ABC", startX, startY, finishX, finishY, abcb);
					uno.currentImg = letSel - 1;
					finishY += 100;
					startY += 100;
					rankings.add(uno);
					ABC dos = new ABC("ABC", startX, startY, finishX, finishY, abc);
					dos.currentImg = letSel;
					finishY += 100;
					startY += 100;
					rankings.add(dos);
					ABC tre = new ABC("ABC", startX, startY, finishX, finishY, abcb);
					tre.currentImg = letSel + 1;
					rankings.add(tre);
				}
				if (pressed != 's') {
					if ((w.getPressedKeys().contains('s'))) {
						pressed = 's';
						if (letSel == 25) {
							letSel = 0;
						} else {
							letSel++;
						}
						deleteRank();
					}
				}
				if (pressed != 'w') {
					if ((w.getPressedKeys().contains('w'))) {
						pressed = 'w';
						if (letSel == 0) {
							letSel = 25;
						} else {
							letSel--;
						}
						deleteRank();
					}
				}
				if (pressed != ' ') {
					if ((w.getPressedKeys().contains(' '))) {
						pressed = ' ';
						String[] eks = abc[letSel].split("\\.");
						nom[nomFin] = eks[0] + "";
						nomFin++;
						deleteRank();
						save.add(new ABC("ABC", startX - 1, startY - 100, finishX, finishY - 100, abc[letSel]));
						accRank += 150;
						letSel = 0;
					}
				}
			}
			if (pressed != '1') {
				if ((w.getPressedKeys().contains('1'))) {
					startX = 100;
					startY = 100;
					finishX = 176;
					finishY = 176;
					deleteRank();
					deleteSave();
					nom[0] = "0";
					nomFin = 0;
					accRank = 0;
					letSel = 0;
				}
			}
			if (pressed != '0') {
				if ((w.getPressedKeys().contains(' ')) && nomFin == 3) {
					nomFin++;
				}
			}
			if (w.getPressedKeys().isEmpty()) {
				pressed = '*';
			}
			sp.addAll(rankings);
			sp.addAll(save);
			f.draw(sp);
			Thread.sleep(20);
		}
		return nom;

	}

	private static void deleteSave() {
		// TODO Auto-generated method stub
		for (Iterator iterator = save.iterator(); iterator.hasNext();) {
			Sprite s = (Sprite) iterator.next();
			iterator.remove();
		}
	}

	private static void deleteRank() {
		// TODO Auto-generated method stub
		for (Iterator iterator = rankings.iterator(); iterator.hasNext();) {
			Sprite s = (Sprite) iterator.next();
			iterator.remove();
		}
	}

	private static void insertarRanking(File fileR, String[] nomRank) throws IOException {
		FileReader in = new FileReader(fileR);
		BufferedReader br = new BufferedReader(in);
		FileWriter out = new FileWriter(fileR, true);
		BufferedWriter bw = new BufferedWriter(out);
		bw.write(punt.puntuacio + ";" + nomRank[0] + " " + nomRank[1] + " " + nomRank[2]);
		bw.newLine();
		bw.flush();
		bw.close();

		ArrayList<Puntuacio> list = new ArrayList<>();

		while (br.ready()) {
			String s = br.readLine();
			String[] ssplit = s.split(";");
			Puntuacio p = new Puntuacio(Integer.parseInt(ssplit[0]),
					ssplit[1].charAt(0) + "" + ssplit[1].charAt(2) + "" + ssplit[1].charAt(4));
			list.add(p);
		}

		Collections.sort(list, Collections.reverseOrder());
		// System.out.println(list);

		FileWriter outF = new FileWriter(fileR);
		BufferedWriter bwF = new BufferedWriter(outF);

		while (!list.isEmpty()) {
			bwF.write(list.get(0).puntuacio + ";" + list.get(0).nombre.charAt(0) + " " + list.get(0).nombre.charAt(1)
					+ " " + list.get(0).nombre.charAt(2));
			list.remove(0);
			bwF.newLine();
		}
		bwF.flush();
		bwF.close();
		br.close();
		bw.close();
	}

	private static void levelPassed(int startX, int startY, int finishX, int finishY, int i, boolean ganado) {
		f.clear();
		w.stopMusic();
		w.playSFX("media/sounds/Win.wav");
		ArrayList<Sprite> sp = new ArrayList<Sprite>();

		for (NPC npccss : Mapa.npcs) {
			npccss.delete();
		}

		for (Iterator iterator = Mapa.npcs.iterator(); iterator.hasNext();) {
			Sprite s = (Sprite) iterator.next();
			if (s.delete) {
				iterator.remove();
				// System.out.println("deleted");
			}
		}

		Mapa.insertHUDandNPCs(startX, startY, finishX, finishX, i, ganado);
		sp = addSprites(sp);

		f.draw(sp);
	}

	private static boolean peachSalvada() {
		if (m.y1 <= 152)
			return true;
		return false;
	}

	private static void levelLost() {
		w.stopMusic();
		w.playSFX("media/sounds/GameOver.wav");
		m.currentImg = 2;
		try {
			Thread.sleep(750);
		} catch (InterruptedException e) {
		}
		m.currentImg = 3;
		try {
			Thread.sleep(1750);
		} catch (InterruptedException e) {
		}
		m.currentImg = 4;
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		m = new Mario("Mario", 108, 768, 132, 800, marioSprites);
		mI = new MarioInv("MarioInv", 100, 767, 132, 800, "");
		t = new Texto("texto", 70, 50, 80, 60, "0");
		acc = 1;
		punt.puntuacio = 0;
		// Delete all NPC and HUD
		for (Iterator iterator = Mapa.npcs.iterator(); iterator.hasNext();) {
			Sprite s = (Sprite) iterator.next();
			iterator.remove();
			// System.out.println("deleted");

		}
		for (Iterator iterator = Mapa.barril.iterator(); iterator.hasNext();) {
			Sprite s = (Sprite) iterator.next();
			iterator.remove();
			// System.out.println("deleted");

		}
	}

	private static void sounds() {
		if (w.getPressedKeys().contains('w')) {
			if (m.jumpCoolSFX == 9 && m.isGrounded(f) && m.estat != 3) {
				w.playSFX("media/sounds/jump.wav");
				m.jumpCoolSFX = 0;
			}
			if (m.estat == 3 && m.movCoolSFX == 9) {
				m.currentImg = 5;
				w.playSFX("media/sounds/mWalk.wav");
				m.movCoolSFX = 0;
			}
		} else if (w.getPressedKeys().contains('d') && m.isGrounded(f)) {
			if (m.movCoolSFX == 9) {
				w.playSFX("media/sounds/mWalk.wav");
				m.movCoolSFX = 0;
			}
		} else if (w.getPressedKeys().contains('a') && m.isGrounded(f)) {
			if (m.movCoolSFX == 9) {
				w.playSFX("media/sounds/mWalk.wav");
				m.movCoolSFX = 0;
			}
		} else if (w.getPressedKeys().contains('s') && m.estat == 3) {
			if (m.movCoolSFX == 9) {
				m.currentImg = 5;
				w.playSFX("media/sounds/mWalk.wav");
				m.movCoolSFX = 0;
			}
		}
	}

	private static ArrayList<Sprite> addSprites(ArrayList<Sprite> sp) {

		sp.addAll(Mapa.suelo);
		sp.addAll(Mapa.subida);
		sp.addAll(Mapa.barril);
		sp.addAll(Mapa.escalera);
		sp.add(m);
		sp.add(mI);
		sp.add(t);
		sp.addAll(Mapa.npcs);
		sp.addAll(Mapa.martillos);
		return sp;
	}

	private static void martilloForm(int martilloTime) {
		boolean martillo = m.martilloTocado(f);
		if (martillo && martillo != mar) {
			mar = martillo;
			m.x1 -= 35;
			m.y1 -= 15;
			w.stopMusic();
			w.playMusic("media/sounds/hammer.wav");
		}
		if (!martillo && martilloTime == 500) {
			mar = false;
			m.martillo = false;
			m.currentImg = 0;
			m.x1 += 35;
			m.y1 += 15;
			w.stopMusic();
			w.playMusic("media/sounds/bkMusic.wav");
		}

	}

	private static int sumarPunto(boolean barrilSaltat, int[] opcSelectArr) {
		barrilSaltat = mI.barrilTocat(f);
		if (m.martillo) {
			for (Barril bb : Mapa.barril) {
				boolean tocado = bb.borrarBarriles(f);
				if (tocado) {
					w.playSFX("media/sounds/pointsGain.wav");
					double suma = 300;
					int acumu = 300;
					if (opcSelectArr[0] == 0 && opcSelectArr[1] == 0 && opcSelectArr[2] == 0)
						suma = acumu * 0.7;
					else if ((opcSelectArr[0] == 0 && opcSelectArr[1] == 0)
							|| (opcSelectArr[0] == 0 && opcSelectArr[2] == 0)
							|| (opcSelectArr[2] == 0 && opcSelectArr[1] == 0))
						suma = acumu * 0.8;
					else if (opcSelectArr[0] == 0 || opcSelectArr[1] == 0 || opcSelectArr[2] == 0)
						suma = acumu * 0.9;
					if (opcSelectArr[0] == 2 && opcSelectArr[1] == 2 && opcSelectArr[2] == 2)
						suma = acumu * 1.3;
					else if ((opcSelectArr[0] == 2 && opcSelectArr[1] == 2)
							|| (opcSelectArr[0] == 2 && opcSelectArr[2] == 2)
							|| (opcSelectArr[2] == 2 && opcSelectArr[1] == 2))
						suma = acumu * 1.2;
					else if (opcSelectArr[0] == 2 || opcSelectArr[1] == 2 || opcSelectArr[2] == 2)
						suma = acumu * 1.1;
					punt.puntuacio += suma;
					acc = punt.puntuacio;
					int lol = punt.puntuacio;
					String punt = Integer.toString(lol);
					t = new Texto("texto", 70, 50, 80, 60, punt);
				}
			}
		} else if (barrilSaltat && acc != punt.puntuacio) {
			w.playSFX("media/sounds/pointsGain.wav");
			double suma = 100;
			int acumu = 100;
			if (opcSelectArr[0] == 0 && opcSelectArr[1] == 0 && opcSelectArr[2] == 0)
				suma = acumu * 0.7;
			else if ((opcSelectArr[0] == 0 && opcSelectArr[1] == 0) || (opcSelectArr[0] == 0 && opcSelectArr[2] == 0)
					|| (opcSelectArr[2] == 0 && opcSelectArr[1] == 0))
				suma = acumu * 0.8;
			else if (opcSelectArr[0] == 0 || opcSelectArr[1] == 0 || opcSelectArr[2] == 0)
				suma = acumu * 0.9;
			if (opcSelectArr[0] == 2 && opcSelectArr[1] == 2 && opcSelectArr[2] == 2)
				suma = acumu * 1.3;
			else if ((opcSelectArr[0] == 2 && opcSelectArr[1] == 2) || (opcSelectArr[0] == 2 && opcSelectArr[2] == 2)
					|| (opcSelectArr[2] == 2 && opcSelectArr[1] == 2))
				suma = acumu * 1.2;
			else if (opcSelectArr[0] == 2 || opcSelectArr[1] == 2 || opcSelectArr[2] == 2)
				suma = acumu * 1.1;
			punt.puntuacio += suma;
			acc = punt.puntuacio;
			int lol = punt.puntuacio;
			String punt = Integer.toString(lol);
			t = new Texto("texto", 70, 50, 80, 60, punt);
		}

		if (!barrilSaltat)
			acc = 1;
		return punt.puntuacio;
	}

	private static boolean gravedad(boolean barrilToc) {
		mI.movv(f);
		boolean tp = m.mGrounded(f);
		if (tp)
			mI = new MarioInv("MarioInv", m.x1, m.y1, m.x2, m.y2, "");
		return m.movv(f);
	}

	private static void input(boolean pressedP, int[] opcSelectArr, int i) throws IOException {
		// devuelve un set con todas las teclas apretadas

		if (w.getPressedKeys().contains('a')) {
			m.moveIzq(f, opcSelectArr);
			mI.moveIzq(f, opcSelectArr);
		} else if (w.getPressedKeys().contains('d')) {
			m.moveDer(f, opcSelectArr);
			mI.moveDer(f, opcSelectArr);
		}
		if (w.getPressedKeys().contains('w')) {
			m.jump(f);
			if (!m.martillo) {
				m.escalar();
				mI.escalar();
			}
		}
		if (w.getPressedKeys().contains('s') && !m.martillo) {
			m.down();
			mI.down();
		}
		if (w.getPressedKeys().isEmpty()) {
			pressedP = false;
		} else if (!pressedP) {
			if (w.getPressedKeys().contains('p')) {
				saveActualGame(opcSelectArr,i);
			}
			pressedP = true;
		}
	}

	private static void saveActualGame(int[] opcSelectArr, int i) throws IOException {
		File f = new File("recources/savegame.sv");
		if (!f.exists()) {
			f.createNewFile();
		}
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		// Save sprites first
		oos.writeObject(m);
		oos.writeObject(mI);
		oos.writeObject(t);
		oos.writeObject(Mapa.barril);
		oos.writeObject(punt.puntuacio);
		oos.writeObject(opcSelectArr);
		oos.writeObject(i);
		oos.flush();
		oos.close();
	}

}