package proyectodkuf2;

import java.io.Serializable;

public abstract class Objetos extends Sprite implements Serializable {

	public Objetos(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
	}

	public Objetos(String name, int x1, int y1, int x2, int y2, String[] path) {
		super(name, x1, y1, x2, y2, path);
	}

}
