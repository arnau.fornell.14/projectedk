package proyectodkuf2;

public abstract class Inmovil extends Objetos{

	public Inmovil(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
	}
	
	public Inmovil(String name, int x1, int y1, int x2, int y2, String[] path) {
		super(name, x1, y1, x2, y2, path);
	}
	
}
