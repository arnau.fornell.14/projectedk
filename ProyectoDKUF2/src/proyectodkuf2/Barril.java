package proyectodkuf2;

import java.util.ArrayList;
import java.util.Random;

public class Barril extends Movil {

	Random r = new Random();
	int estat = 0; // 0 cayendo, 1 suelo 2 escalera
	int estatB = 1; // 1 derecha 2 izq
	boolean dir = false; // true der false izq
	boolean primerBarril = true;
	int baixarS = 2; // 0 true 1 false 2 reset

	public Barril(String name, int x1, int y1, int x2, int y2, String[] path) {
		super(name, x1, y1, x2, y2, path);
	}

	public void tocandoSuelo(Field f, int[] opcSelectArr) {
		if (estat == 0) {
			baixarS = 2;
			if (estatB == 1) {
				if (dir) {
					estatB = 2;
					currentImg = 1;
				} else {
					currentImg = 0;
					estatB = 1;
				}

			} else if (estatB == 2) {
				if (!dir) {
					currentImg = 0;
					estatB = 1;
				} else {
					currentImg = 1;
					estatB = 2;
				}
			}
		}
		if (isGrounded(f) && estatB == 1 && estat != 2) {
			getGrounded(f);
			if(opcSelectArr[1]==0) {
				x1+=2;
				x2+=2;
			}
			else if(opcSelectArr[1]==2) {
				x1 += 4;
				x2 += 4;
			} else {
				x1 += 3;
				x2 += 3;
			}
			estat = 1;
			dir = true;
		} else if (isGrounded(f) && estatB == 2 && estat != 2) {
			getGrounded(f);
			if(opcSelectArr[1]==0) {
				x1-=2;
				x2-=2;
			}
			else if(opcSelectArr[1]==2) {
				x1 -= 4;
				x2 -= 4;
			} else {
				x1 -= 3;
				x2 -= 3;
			}
			estat = 1;
			dir = false;
		} else if (!isGrounded(f) && estat != 2) {
			estat = 0;
		} else if (estat == 2 && isGrounded(f)) {
			estat = 0;
		}

	}

	public void caidaEscalera(Field f) {
		ArrayList<Sprite> ls = collidesWithField(f);
		for (Sprite s : ls) {
			y1++;
			y2++;
			if (s instanceof Escalera) {
				if (baixarS == 2)
					baixarS = r.nextInt(2);
				if (this.stepsOn(s)) {
					if (baixarS == 0)
						estat = 2;
				}
			}
		}
	}

	public void caidaBarril(Field f) {
		if (estat == 0) {
			y1 += 2;
			y2 += 2;
		} else {
			y1++;
			y2++;
		}
		if (y2 >= 900) {
			delete();
		}
	}

	public boolean borrarBarriles(Field f) {
		ArrayList<Sprite> ls = collidesWithField(f);
		boolean tocado = false;
		for (Sprite s : ls) {
			if (s instanceof Mario) {
				delete();
				tocado = true;
			}
		}
		return tocado;
	}
	
}
