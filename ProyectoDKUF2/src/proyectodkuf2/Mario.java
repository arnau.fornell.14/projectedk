package proyectodkuf2;

import java.util.ArrayList;

public class Mario extends Aliados {

	char direccion = 'd';
	int cooldown = 20;
	int estat = 0; // 0 suelo, 1 subiendo 2 cayendo 3 escalando
	int maxSalto = 25;
	boolean tocSuelo = false;
	int movCoolSFX = 9;
	int jumpCoolSFX = 9;
	boolean inEscalera;
	boolean martillo = false;

	public Mario(String name, int x1, int y1, int x2, int y2, String[] marioSprites) {
		super(name, x1, y1, x2, y2, marioSprites);
	}

	public void moveIzq(Field f, int[] opcSelectArr) {
		if (estat == 0 || estat == 1 || estat == 2) {
			if(opcSelectArr[0]==0) {
				x1--;
				x2--;
			}
			else if(opcSelectArr[0]==2) {
				x1 -= 3;
				x2 -= 3;
			} else {
				x1 -= 2;
				x2 -= 2;
			}
			direccion = 'i';
			currentImg = martillo ? 7 : 1;
			// currentImg = 1;
			if (movCoolSFX < 9)
				movCoolSFX++;
		}
		if (estat == 3 && isGrounded(f)) {
			if(opcSelectArr[0]==0) {
				x1--;
				x2--;
			}
			else if(opcSelectArr[0]==2) {
				x1 -= 3;
				x2 -= 3;
			} else {
				x1 -= 2;
				x2 -= 2;
			}
			direccion = 'i';
			getGrounded(f);
			estat = 0;
			currentImg = martillo ? 7 : 1;
			// currentImg = 1;
			if (movCoolSFX < 9)
				movCoolSFX++;
		}
	}

	public void moveDer(Field f, int[] opcSelectArr) {
		if (estat == 0 || estat == 1 || estat == 2) {
			if(opcSelectArr[0]==0) {
				x1++;
				x2++;
			}
			else if(opcSelectArr[0]==2) {
				x1 += 3;
				x2 += 3;
			} else {
				x1 += 2;
				x2 += 2;
			}
			direccion = 'd';
			currentImg = martillo ? 6 : 0;
			// currentImg = 0;
			if (movCoolSFX < 9)
				movCoolSFX++;
		}
		if (estat == 3 && isGrounded(f)) {
			if(opcSelectArr[0]==0) {
				x1++;
				x2++;
			}
			else if(opcSelectArr[0]==2) {
				x1 += 3;
				x2 += 3;
			} else {
				x1 += 2;
				x2 += 2;
			}			
			direccion = 'd';
			getGrounded(f);
			estat = 0;
			currentImg = martillo ? 6 : 0;
			// currentImg = 0;
			if (movCoolSFX < 9)
				movCoolSFX++;
		}
	}

	public boolean movv(Field f) {
		boolean test = false;
		inEscalera = false;
		ArrayList<Sprite> ls = collidesWithField(f);
		if (estat == 1 && estat != 3) {
			y1 = y1 - 2;
			y2 = y2 - 2;
			maxSalto--;
			cooldown = 0;
			if (jumpCoolSFX < 9)
				jumpCoolSFX++;
			if (maxSalto == 0) {
				estat = 2;
			}
		} else if (isGrounded(f) && estat != 3) {
			for (Sprite s : ls) {
				if (MainJuego.w.getPressedKeys().contains('s')) {
					y1++;
					y2++;
				}
				if (MainJuego.w.getPressedKeys().contains('w')) {
					y1--;
					y2--;
				}
				if (s instanceof Terreno) {
					if (this.stepsOn(s)) {
						getGrounded(f);
					}
				}
			}
			estat = 0;
			maxSalto = 25;

		} else if (estat != 3) {
			estat = 2;
			y1 = y1 + 2;
			y2 = y2 + 2;
			if (y2 >= 1000) {
				return true;
			}
		}
		tocSuelo = false;

		for (Sprite s : ls) {
			// System.out.println(s);
			if (s instanceof Escalera) {
				inEscalera = true;
					if(!martillo) {
					estat = 3;
					if (jumpCoolSFX < 9)
						jumpCoolSFX++;
					
				}
			} else if (s instanceof Terreno) {
				if (this.stepsOn(s)) {
					estat = 0;
				}
			}
			if (s instanceof Rampa) {
				getGrounded(f);
			}
			if (s instanceof Barril && !martillo) {
				return true;
			} else {
				test = false;
			}
		}
		return test;
	}

	public void destruir(Field f) {
	
	}

	public void jump(Field f) {
		if (estat == 0 && cooldown == 20) {
			estat = 1;
		}
	}

	public void update() {
		if (cooldown < 20)
			cooldown++;
	}

	public void down() {
		if (movCoolSFX < 9)
			movCoolSFX++;
		if (estat == 3 && !martillo) {
			y2 += 1;
			y1 += 1;
		}
	}

	public void escalar() {
		if (movCoolSFX < 9)
			movCoolSFX++;
		if (estat == 3 && inEscalera && !martillo) {
			y2 -= 1;
			y1 -= 1;
		}
	}

	public boolean mGrounded(Field f) {
		if (isGrounded(f))
			return true;
		return false;
	}

	public boolean martilloTocado(Field f) {
		ArrayList<Sprite> ls = collidesWithField(f);
		for (Sprite s : ls) {
			// System.out.println(s);
			if (s instanceof Martillo) {
				martillo = true;
				currentImg = 6;
				return true;
			}
		}
		return false;
	}
}
