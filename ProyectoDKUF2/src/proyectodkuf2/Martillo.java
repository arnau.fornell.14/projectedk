package proyectodkuf2;

import java.util.ArrayList;

public class Martillo extends Inmovil {

	public Martillo(String name, int x1, int y1, int x2, int y2, String path) {
		super(path, x1, y1, x2, y2, path);

	}
	
	public void tocado (Field f) {
		ArrayList<Sprite> ls = collidesWithField(f);
		for (Sprite s : ls) {
			if(s instanceof Mario) {
				delete();
			}
		}
		
	}
		
}
