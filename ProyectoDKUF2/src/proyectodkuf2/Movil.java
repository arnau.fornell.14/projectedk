package proyectodkuf2;

public abstract class Movil extends Objetos {

	public Movil(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
	}

	public Movil(String name, int x1, int y1, int x2, int y2, String[] path) {
		super(name, x1, y1, x2, y2, path);
	}

}
